/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package filesmanagement;

import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.*;
import java.io.*;
import java.text.DateFormat;
import java.util.Scanner;

/**
 *
 * @author Lionel Mangoua
 */
public class LogFile {
  
    //Display date and Time(working)
    //<editor-fold defaultstate="collapsed" desc="dateAndTime">
    public static void dateAndTime() {
        
        try {
            Calendar cal = Calendar.getInstance();
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss.SS");
            String strDate = sdf.format(cal.getTime());
            System.out.println("Date and Time: " + strDate);
        } catch (Exception e) {
            System.out.println("ERROR!!");
        }
    }
//</editor-fold>
    
    //Display file (or directory) info.(working)
    //<editor-fold defaultstate="collapsed" desc="fileAndDirectoryInfo">
    public static void fileAndDirectoryInfo() throws IOException {
        
        Scanner input = new Scanner(System.in);
        
        System.out.println("Enter file or directory name: ");
        
        //Create Path Object based on User Input
        Path path = Paths.get(input.nextLine());
        
        if (Files.exists(path)) { //if path exists, output info about it
            //display file (or directory) info.
            System.out.printf("%n%s exists%n ", path.getFileName());
            System.out.printf("%s a directory%n ",
                    Files.isDirectory(path) ? "Is" : "Is not");
            System.out.printf("Last modified: %s%n ",
                    Files.getLastModifiedTime(path));
            System.out.printf("File store: %s%n ",
                    Files.getFileStore(path));
            System.out.printf("Path: %s%n ", path);
            
            dateAndTime();
            
            if (Files.isDirectory(path)) {//output directory
                System.out.printf("%nDirectory content:%n ");
                
                //Object for iterating through a directory's contents
                DirectoryStream<Path> directoryStream = Files.newDirectoryStream(path);
                
                for (Path p : directoryStream) {
                    System.out.println(p);
                }
            }
            else { //no file or directory, output error message
                System.out.printf("%s does not exist%n ", path);
            }
            System.out.println("***********************************************************");
            System.out.println();
            System.out.println();
            
        }
    } //End of fileAndDirectoryInfo
//</editor-fold>
    
    //Read all files in folder
    //<editor-fold defaultstate="collapsed" desc="readAllFilesEntry">
    public static void readAllFilesEntry() {
        
        try {
            
            FilenameFilter filter = new FilenameFilter() {
                public boolean accept(File dir, String name) {
                    return name.endsWith(".dat");
                }
            };
            
            File folder = new File("C:\\Users\\lmangoua\\Documents\\Lionel_Java_Projects\\CodeRepository\\FileManagement_Repository\\FilesManagement\\binary_files");
            File[] listOfFiles = folder.listFiles(filter);
            
            for (int i = 0; i < listOfFiles.length; i++) {
                File file = listOfFiles[i];
                String content = FileUtils.readFileToString(file);

                // do something with the file
                //                duplicates(listOfFiles);
//                for (int j = 0; j < listOfFiles.length; j++) {
//                    for (int k = j + 1; k < listOfFiles.length; k++) {
//                        if (k != j && listOfFiles[k].equals(listOfFiles[j])) { 
//                            System.out.println("File " + k + " is duplicate of File " + j);
//
//                        }
//                    }
//                }//End 2nd for loop
                for (int j = 0; j < listOfFiles.length; j++) {
                    for (int k = j + 1; k < listOfFiles.length; k++) {
                        if (k != j && listOfFiles[k].equals(listOfFiles[j])) { 
                            System.out.println("File " + k + " is duplicate of File " + j);
                            
                        } else {
                            System.out.println("There is NO duplicate.");
                        }
                    }
                }//End 2nd for loop

            }//End 1st for loop
        } catch (Exception e) {
            System.out.println("Error finding duplicates");
        }
        
    }
//</editor-fold>
    
    //Finding duplicates
    //<editor-fold defaultstate="collapsed" desc="duplicates">
    static boolean duplicates(final File[] listOfFiles) {
        
        for (int j = 0; j < listOfFiles.length; j++) {
            for (int k = j + 1; k < listOfFiles.length; k++) {
                if (k != j && listOfFiles[k].equals(listOfFiles[j])) { // or use .equals()
                    System.out.println("File " + k + " is duplicate of File " + j);
                    return true;
                }
                
            }
            
        }
        System.out.println("There is NO duplicate");
        return false;
    }
//</editor-fold>
    
    //Read all files in folder(Test)
    //<editor-fold defaultstate="collapsed" desc="readAllFileEntry">
    public static void readAllFileEntry() throws IOException {
        
        File folder = new File("C:\\Users\\lmangoua\\Documents\\Lionel_Java_Projects\\CodeRepository\\FileManagement_Repository\\FilesManagement\\binary_files");
        File[] listOfFiles = folder.listFiles();
        
        System.out.println("directory: " + folder);
        System.out.println("Files:");
        for (File file : listOfFiles) {
            if (file.isFile()) {
                System.out.println(file.getName());
            }
        }
        
        System.out.println("Displaying duplicates");
        duplicates(listOfFiles);
    }
//</editor-fold>
    
    //List files(working)
    //<editor-fold defaultstate="collapsed" desc="listFilesFromFolder">
    public static void listFilesFromFolder() {
        
        File folder = new File("C:\\Users\\lmangoua\\Documents\\Lionel_Java_Projects\\CodeRepository\\FileManagement_Repository\\FilesManagement\\binary_files");
        File[] listOfFiles = folder.listFiles();
        
        System.out.println("directory: " + folder);
        System.out.println("Files:");
        for (File file : listOfFiles) {
            if (file.isFile()) {
                System.out.println(file.getName());
                System.out.println();
                System.out.println();
            }
        }
    }
//</editor-fold>
    
    //generate log.txt
    //<editor-fold defaultstate="collapsed" desc="writeLogFile">
    public static void writeLogFile() {
        
        BufferedWriter writer = null;
        try {
            writer = new BufferedWriter(new FileWriter("C:\\Users\\lmangoua\\Documents\\Lionel_Java_Projects\\CodeRepository\\FileManagement_Repository\\FilesManagement\\binary_files\\log.txt"));
            Calendar cal = Calendar.getInstance();
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss.SS");
            String strDate = sdf.format(cal.getTime());
            System.out.println("Date and Time: " + strDate);
            writer.write("Date and Time: " + strDate);

        } 
        catch (IOException e) {
            System.out.println("ERROR detected!");
        } 
        finally {
            try {
                if (writer != null) {
                    writer.close();
                }
            } 
            catch (IOException e) {
                System.out.println("ERROR! detected!");
            }
        }
        System.out.println("===========================");
        System.out.println("=Done writing on log file.=");
        System.out.println("===========================");
        System.out.println();
        System.out.println();

    }
//</editor-fold>

    
}
